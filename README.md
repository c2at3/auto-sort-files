# Auto sort files
Make the folders on your computer neater 🤣.
## Tutorials sortFile.py (v1) :
- This version uses extensions (file type) to auto-sort files into folders.
- If the file name is duplicated, it will not be sort.

**For example:**

I have a folder tree such as:
```
+Downloads
|----funny_img.png
|----sad_img.png
|----IDA_pro.exe
|----Project_folder
|----game.zip
|----sortFile.py
```
After running the file with the command:
```
python sortFile.py
```
or:
```
python3 sortFile.py
```
**Result:**
```
+Downloads_folder
|----png_file
|    ----funny_img.png
|        sad_img.png
|----exe_file
|    ----IDA_pro.exe
|----Project_folder
|----zip_file
|    ----game.zip
|----py_file
|    ----sortFile.py
```
## Tutorials sortFilev2.py (v2) :
- This version auto-sort Files and Folders based on extensions, file name, folder name configured in config_folder.YAML file.
- If the file name is duplicated, it will not be sort.

**[config_folder.yaml](https://gitlab.com/c2at3/auto-sort-files/-/blob/main/config_folder.yaml) code:**
```yaml
groupfile:
- Malware_analytics:
  - "[MAL]"
- Meme_image:
  - "[MEME]"

foldername:
- Microsoft_Word:
  - doc
  - docx
  - dotm
- Image:
  - png
  - jpg
```
`groupfile`: Group files and folders with the same title together(Ex: [MAL] IDA_pro.exe has the same title as [MAL] mal.png
).

`foldername`: Sort File based on extension.

***NOTE: `groupfile` is sorted first then `foldername`.***

**For example:**

With the content in the [config_folder.yaml](https://gitlab.com/c2at3/auto-sort-files/-/blob/main/config_folder.yaml) file as above and the folder tree as below:
```
+Downloads_folder
|----[MEME] funny_img.png
|----[MEME] sad_img.jpg
|----[MAL] IDA_pro.exe
|----[MAL] Malware_sample_folder
|----document1111.docx
|----document2222.doc
|----game.zip
|----Project_folder
|----sortFilev2.py
|----config_folder.YAML
```
After running the file with the command:
```
python sortFilev2.py
```
or:
```
python3 sortFilev2.py
```
**Result:**
```
+Downloads_folder
|----Meme_image
|    ----[MEME] funny_img.png
|        [MEME] sad_img.png
|----Malware_analytics
|    ----[MAL] IDA_pro.exe
|        [MAL] Malware_sample_folder
|----Microsoft_Word
|    ----document1111.docx
|        document2222.doc
|----game.zip
|----Project_folder
|----sortFilev2.py
|----config_folder.YAML
```
[Read more](https://gitlab.com/c2at3/auto-sort-files/-/blob/main/config_folder.yaml#L2-18) comments in the file.
> In addition to the default configuration, you can customize the [config_folder.yaml](https://gitlab.com/c2at3/auto-sort-files/-/blob/main/config_folder.yaml) file to create folders containing the extensions or filenames as you want.
