import os
import glob
import yaml
import re

def creatDir(dir):
    try:
        os.makedirs(dir)
        print('Folder "{}" has been created.'.format(dir))
    except FileExistsError:
        print('Folder "{}" already exists.'.format(dir))

def moveDir(file_name, folder):
    try:
        os.rename(file_name, '{}/{}'.format(folder, file_name))
        print('Move "{}" --> "{}/{}".'.format(file_name, folder, file_name))
    except (OSError, IndexError):
        print('Don\'t Move "{}" --> "{}/{}".'.format(file_name, folder, file_name))

with open('config_folder.yaml', 'r', encoding='utf8') as config_folder:
    data = config_folder.read()
    struct_folder = yaml.safe_load(data)

listdir = glob.glob('*')
print('========================START: LIST DIRECTORY====================')
print(listdir)
print('========================END: LIST DIRECTORY======================')

extensions = []

for e in listdir:
    if len(e.split('.')) > 1:
        extensions.append(e.split('.')[-1])
extensions_listdir = set(extensions)
# print(extensions_listdir)

regex = r"(\[.*\])"
title_listdir = set()
for file_name in listdir:
    match = re.search(regex, file_name)
    if match:
        title_listdir.add(match.group())
# print(title_listdir)

# xử lý các Folder theo title trong [] tuỳ chỉnh
print('========================START: GROUP FILE========================')
groupfile = struct_folder['groupfile']
for item in groupfile:
    for folder, titles in item.items():
        # Tạo thư mục
        for title in title_listdir:
            print(title)
            if title in titles:
                creatDir(folder)

        # Di chuyển thư mục
        for file_name in listdir:
            match = re.search(regex, file_name)
            if match and match.group() in titles:
                moveDir(file_name, folder)
print('========================END: GROUP FILE==========================')

print('========================START: EXTENSIONS FILE===================')
# xử lý các Folder theo phần đuôi mở rộng
foldername = struct_folder['foldername']
for item in foldername:
    for folder, extensions in item.items():
        extensions = [e.lower() for e in extensions]
        # Tạo thư mục
        for i in extensions_listdir:
            if i in extensions:
                creatDir(folder)

        # Di chuyển thư mục
        for file_name in listdir:
            if len(file_name.split('.')) > 1:
                if file_name.split('.')[-1].lower() in extensions:
                    moveDir(file_name, folder)
print('========================END: EXTENSIONS FILE=====================')
